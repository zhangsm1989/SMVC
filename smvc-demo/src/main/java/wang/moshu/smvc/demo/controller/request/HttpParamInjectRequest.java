package wang.moshu.smvc.demo.controller.request;

/**
 * HTTP参数注入请求
 * 
 * @category HTTP参数注入请求
 * @author xiangyong.ding@weimob.com
 * @since 2017年1月23日 下午9:07:29
 */
public class HttpParamInjectRequest
{
	private String name;

	private String id;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

}
