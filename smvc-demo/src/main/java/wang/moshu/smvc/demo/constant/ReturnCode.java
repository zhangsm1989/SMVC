package wang.moshu.smvc.demo.constant;

/**
 * 业务异常返回码
 * 
 * @category 业务异常返回码
 * @author xiangyong.ding@weimob.com
 * @since 2016年11月8日 上午10:40:35
 */
public interface ReturnCode
{
	/**
	 * 未登陆
	 */
	String NO_LOGIN = "000002";
}
