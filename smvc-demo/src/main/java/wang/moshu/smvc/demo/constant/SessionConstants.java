package wang.moshu.smvc.demo.constant;

/**
 * session 常量
 * 
 * @author dingxiangyong 2016年8月9日 下午10:25:29
 */
public interface SessionConstants
{
	/**
	 * 用户登录回话ID
	 */
	String SESSION_USER = "user";
}
