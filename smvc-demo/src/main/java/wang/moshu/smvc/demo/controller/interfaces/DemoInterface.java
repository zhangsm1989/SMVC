package wang.moshu.smvc.demo.controller.interfaces;

import org.springframework.stereotype.Controller;

import wang.moshu.smvc.demo.controller.response.ReturnDataResponse;
import wang.moshu.smvc.demo.intercept.ExecuteTimeInterceptor;
import wang.moshu.smvc.framework.annotation.RequestMapping;
import wang.moshu.smvc.framework.enums.ReturnType;
import wang.moshu.smvc.framework.exception.BusinessException;
import wang.moshu.smvc.framework.interceptor.annotation.Intercept;
import wang.moshu.smvc.framework.multipart.MultipartFile;
import wang.moshu.smvc.framework.util.Assert;

/**
 * 接口路由
 * 
 * @category 接口路由
 * @author xiangyong.ding@weimob.com
 * @since 2017年1月23日 下午9:32:49
 */
@Controller
@RequestMapping(value = "/i/")
@Intercept(value = { ExecuteTimeInterceptor.class })
public class DemoInterface
{
	/**
	 * 无返回值接口，框架会返回成功返回码：{ "code":0, "data":null, "message":"", "returnCode":""
	 * }
	 * 
	 * @category 无返回值接口，框架会返回成功
	 * @author xiangyong.ding@weimob.com
	 * @since 2017年1月23日 下午9:34:55
	 */
	@RequestMapping(value = "noReturn", returnType = ReturnType.JSON)
	public void noReturn()
	{
		// do nothing
	}

	/**
	 * 有异常抛出的接口，框架会返回失败返回码：{ "code":1, "data":"", "message":"",
	 * "returnCode":"000001" }
	 * 
	 * @category 有异常抛出的接口，框架会返回失败返回码：{ "code":1, "data":"", "message":"",
	 *           "returnCode":"000001" }
	 * @author xiangyong.ding@weimob.com
	 * @since 2017年1月23日 下午9:34:55
	 */
	@RequestMapping(value = "throwException", returnType = ReturnType.JSON)
	public void throwException()
	{
		// 直接抛出异常
		throw new BusinessException("101010", "接口抛出异常");

	}

	/**
	 * 业务service即加上注解就是接口，返回值就是json。
	 * <p>
	 * url : http://localhost:8080/smvc-demo/i/returnData
	 * </p>
	 * <p>
	 * 返回json : { "code":0, "data":"smvc maybe good." , "message":"",
	 * "returnCode":"" }
	 * </p>
	 * 
	 * @category 业务service即加上注解就是接口，返回值就是json
	 * @author xiangyong.ding@weimob.com
	 * @since 2017年1月23日 下午9:34:55
	 */
	@RequestMapping(value = "returnData", returnType = ReturnType.JSON)
	public String returnData()
	{
		// 直接抛出异常
		return "smvc maybe good.";
	}

	/**
	 * 业务service即加上注解就是接口，返回值就是json。
	 * <p>
	 * url : http://localhost:8080/smvc-demo/i/returnDataObj?name=dingxy&id=1234
	 * </p>
	 * <p>
	 * 返回json : { "code":0, "data":{ "id":1234, "name":"dingxy" }, "message":"",
	 * "returnCode":"" }
	 * </p>
	 * 
	 * @category 业务service即加上注解就是接口，返回值就是json
	 * @author xiangyong.ding@weimob.com
	 * @since 2017年1月23日 下午9:34:55
	 */
	@RequestMapping(value = "returnDataObj", returnType = ReturnType.JSON)
	public ReturnDataResponse returnDataObj(String name, Integer id)
	{
		Assert.notNull(name);
		Assert.notNull(id);

		ReturnDataResponse response = new ReturnDataResponse();
		response.setName(name);
		response.setId(id);

		return response;
	}

	/**
	 * 单个文件上传支持，只需要将文件的字段名称作为接口参数（你可以作为自定义bean的字段），smvc会自动将文件上传对象放入参数中
	 * 
	 * @category 单个文件上传支持
	 * @author xiangyong.ding@weimob.com
	 * @since 2017年1月23日 下午9:57:46
	 */
	@RequestMapping(value = "oneFileUpload", returnType = ReturnType.JSON)
	public String oneFileUpload(MultipartFile file)
	{
		Assert.notNull(file);

		return "您上传的文件为：" + file.getOriginalFilename();
	}

	/**
	 * 多个文件上传支持，只需要将文件的字段名称作为接口参数数组（你可以作为自定义bean的数组字段），smvc会自动将文件上传对象放入参数中
	 * 
	 * @category 多个文件上传支持
	 * @author xiangyong.ding@weimob.com
	 * @since 2017年1月23日 下午9:57:46
	 */
	@RequestMapping(value = "multiFileUpload", returnType = ReturnType.JSON)
	public String multiFileUpload(MultipartFile[] file)
	{
		Assert.notNull(file);

		return "您一共上传了[" + file.length + "]个文件";
	}

}
