package wang.moshu.smvc.demo.controller.pages;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;

import wang.moshu.smvc.demo.controller.request.HttpParamInjectRequest;
import wang.moshu.smvc.framework.annotation.RequestMapping;
import wang.moshu.smvc.framework.enums.RequestDataType;
import wang.moshu.smvc.framework.enums.ReturnType;
import wang.moshu.smvc.framework.exception.BusinessException;

/**
 * 页面路由
 * 
 * @category @author xiangyong.ding@weimob.com
 * @since 2016年12月1日 上午12:04:16
 */
@Controller
@RequestMapping(value = "/p/")
public class DemoPage
{
	/**
	 * 基本的页面匹配路由，返回值必须为string，返回值即页面名字
	 * 
	 * @category 基本的页面匹配路由，返回值必须为string，返回值即页面名字
	 * @author xiangyong.ding@weimob.com
	 * @since 2017年1月23日 下午5:57:57
	 * @return
	 */
	@RequestMapping(value = "", returnType = ReturnType.PAGE)
	public String index()
	{
		// 返回index页面
		return "index";
	}

	/**
	 * 返回值给页面：将参数放入参数中的Map<Object, Object> returnMap中，smvc会自动返回给页面
	 * 
	 * @category 返回值给页面
	 * @author xiangyong.ding@weimob.com
	 * @since 2017年1月23日 下午5:57:57
	 * @return
	 */
	@RequestMapping(value = "returnValue", returnType = ReturnType.PAGE)
	public String returnValue(Map<Object, Object> returnMap)
	{
		returnMap.put("returnValue1", 1);
		returnMap.put("returnValue2", 2);
		// 返回页面
		return "returnValue";
	}

	/**
	 * 路由参数注入，注意：参数类型必须为java基本对象型
	 * 
	 * @category 路由参数注入
	 * @author xiangyong.ding@weimob.com
	 * @since 2017年1月23日 下午5:57:57
	 * @see 参数类型必须为java基本对象型，且类型均正确
	 * @return
	 */
	@RequestMapping(value = "routeParamInject/{name}/{id}", returnType = ReturnType.PAGE)
	public String routeParamInject(String name, Integer id, Map<Object, Object> returnMap)
	{
		returnMap.put("name", name);
		returnMap.put("id", id);
		// 返回页面
		return "routeParamInject";
	}

	/**
	 * HTTP参数注入接口参数(java基本对象性)
	 * ，url:http://localhost:8080/smvc-demo/p/httpParamInject?name=dingxy&id=
	 * 1234
	 * 
	 * @category HTTP参数注入接口参数
	 * @author xiangyong.ding@weimob.com
	 * @since 2017年1月23日 下午5:57:57
	 * @see 参数类型必须为java基本对象型，且类型均正确
	 * @return
	 */
	@RequestMapping(value = "httpParamInject", returnType = ReturnType.PAGE)
	public String httpParamInject(String name, Integer id, Map<Object, Object> returnMap)
	{
		returnMap.put("name", name);
		returnMap.put("id", id);
		// 返回页面
		return "httpParamInject";
	}

	/**
	 * HTTP参数注入接口参数(自定义对象型)
	 * ，url:http://localhost:8080/smvc-demo/p/httpParamInject2?name=dingxy&id=
	 * 1234
	 * 
	 * @category HTTP参数注入接口参数(自定义对象型)
	 * @author xiangyong.ding@weimob.com
	 * @since 2017年1月23日 下午5:57:57
	 * @return
	 */
	@RequestMapping(value = "httpParamInject2", returnType = ReturnType.PAGE)
	public String httpParamInject2(HttpParamInjectRequest request, Map<Object, Object> returnMap)
	{
		Assert.notNull(request);
		returnMap.put("name", request.getName());
		returnMap.put("id", request.getId());
		// 返回页面
		return "httpParamInject";
	}

	/**
	 * JSON参数注入接口参数(用户自定义对象性)
	 * ，url:http://localhost:8080/smvc-demo/p/jsonParamInject?{%22name%22:%
	 * 22dingxy%22,%22id%22:1234}
	 * 
	 * @category JSON参数注入接口参数(用户自定义对象性)
	 * @author xiangyong.ding@weimob.com
	 * @since 2017年1月23日 下午5:57:57
	 * @return
	 */
	@RequestMapping(value = "jsonParamInject", requestDataType = RequestDataType.JSON, returnType = ReturnType.PAGE)
	public String jsonParamInject(HttpParamInjectRequest request, Map<Object, Object> returnMap)
	{
		Assert.notNull(request);
		returnMap.put("name", request.getName());
		returnMap.put("id", request.getId());
		// 返回页面
		return "httpParamInject";
	}

	/**
	 * 
	 * @category @author xiangyong.ding@weimob.com
	 * @since 2017年2月3日 下午4:39:25
	 */
	@RequestMapping(value = "throwException", returnType = ReturnType.PAGE)
	public void throwException()
	{
		// 直接抛出异常
		throw new BusinessException("101010", "接口抛出异常");
	}
}
