package wang.moshu.smvc.demo.controller.interfaces;

import org.springframework.stereotype.Controller;

import wang.moshu.smvc.demo.intercept.ExecuteTimeInterceptor;
import wang.moshu.smvc.demo.intercept.LoginInterceptor;
import wang.moshu.smvc.framework.annotation.RequestMapping;
import wang.moshu.smvc.framework.enums.ReturnType;
import wang.moshu.smvc.framework.interceptor.annotation.Clear;
import wang.moshu.smvc.framework.interceptor.annotation.Intercept;

/**
 * 拦截器例子接口
 * 
 * @category 拦截器例子接口
 * @author xiangyong.ding@weimob.com
 * @since 2017年1月26日 上午11:45:44
 */
@Controller
@RequestMapping(value = "/i/")
@Intercept(value = { LoginInterceptor.class })
public class DemoInterceptInterface
{
	/**
	 * class上面已经加上的拦截器，如果方法上没有说明，那默认都需要走
	 * 
	 * @category class上面已经加上的拦截器，如果方法上没有说明，那默认都需要走
	 * @author xiangyong.ding@weimob.com
	 * @since 2017年1月26日 上午11:47:32
	 */
	@RequestMapping(value = "needLogin", returnType = ReturnType.JSON)
	public void needLogin()
	{
		// do nothing
	}

	/**
	 * class上面已经加上的拦截器，方法上可以@clear来擦除
	 * 
	 * @category class上面已经加上的拦截器，方法上可以@clear来擦除
	 * @author xiangyong.ding@weimob.com
	 * @since 2017年1月26日 上午11:48:58
	 */
	@RequestMapping(value = "noNeedLogin", returnType = ReturnType.JSON)
	@Clear(value = { LoginInterceptor.class })
	public void noNeedLogin()
	{
		// 该接口执行不需要登陆
	}

	/**
	 * 接口方法上可以添加class上未添加的拦截器
	 * 
	 * @category 接口方法上可以添加class上未添加的拦截器
	 * @author xiangyong.ding@weimob.com
	 * @since 2017年1月26日 下午12:02:00
	 */
	@RequestMapping(value = "executeTime", returnType = ReturnType.JSON)
	@Clear(value = { LoginInterceptor.class })
	@Intercept(value = { ExecuteTimeInterceptor.class })
	public void executeTime()
	{
		// 该接口会被ExecuteTimeInterceptor拦截
	}
}
