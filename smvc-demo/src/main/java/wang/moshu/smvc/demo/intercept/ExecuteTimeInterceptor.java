package wang.moshu.smvc.demo.intercept;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import wang.moshu.smvc.framework.interceptor.RequestInterceptor;

/**
 * 执行时间拦截器
 * 
 * @author dingxiangyong 2016年8月12日 下午2:49:58
 */
@Component
public class ExecuteTimeInterceptor implements RequestInterceptor
{
	private ThreadLocal<Long> startTime = new ThreadLocal<Long>();

	private Log logger = LogFactory.getLog(ExecuteTimeInterceptor.class);

	@Override
	public String description()
	{
		return "接口时间计算拦截器";
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		startTime.set(System.currentTimeMillis());
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object returnObj) throws Exception
	{

	}

	@Override
	public void commitHandle(HttpServletRequest request, HttpServletResponse response)
	{
		if (startTime.get() != null)
		{
			logger.error("接口时间：" + (System.currentTimeMillis() - startTime.get()));
		}
	}

}
