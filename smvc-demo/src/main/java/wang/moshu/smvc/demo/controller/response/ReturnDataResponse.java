package wang.moshu.smvc.demo.controller.response;

public class ReturnDataResponse
{
	private String name;

	private Integer id;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

}
