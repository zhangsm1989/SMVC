/**
 * @filename MappingType.java
 * @createtime 2015.7.12
 * @author Big Martin
 * @comment 
 */
package wang.moshu.smvc.framework.enums;

/**
 * Mapping type : class type or method type
 * @author Big Martin
 *
 */
public enum MappingType {
    CLASS, METHOD
}
