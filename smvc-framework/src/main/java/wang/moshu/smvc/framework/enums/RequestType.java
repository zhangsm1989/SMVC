package wang.moshu.smvc.framework.enums;

/**
 * 请求方式，get/post
 * 
 * @author dingxiangyong 2016年8月7日 下午10:00:06
 */
public enum RequestType
{
	GET, POST
}
