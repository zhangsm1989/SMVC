package wang.moshu.smvc.framework.enums;

/**
 * 返回类型，JSON/PAGE
 * 
 * @author dingxiangyong 2016年7月13日 下午8:33:53
 */
public enum ReturnType
{
	JSON, PAGE, NOTHING
}
