package wang.moshu.smvc.framework.enums;

/**
 * 请求数据类型
 * 
 * @author dingxiangyong 2016年7月18日 下午10:25:10
 */
public enum RequestDataType
{
	JSON, FORMDATA
}
