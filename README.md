# SMVC
====================

![License](https://img.shields.io/cocoapods/l/TWPhotoPicker.svg)
![Platform](https://img.shields.io/badge/platform-java-yellow.svg)
> **SMVC** 是基于java开发的轻量级MVC框架，框架依赖于spring-core。 

框架开发宗旨：业务方法加上注解就是接口。
框架开发目的：该框架原型是为了学习spring而写的demo（原型，https://github.com/feichendingxiangyong/Smvc
，该框架在原型基础做了优化和简化），但后来该框架在实际项目中得到了应用，该框架具有如下特点： 

* 框架无入侵，只要注解即可完成路由配置；
* 路由配置灵活，业务service即加上注解就是接口，返回值就是json或者页面名字； 
* HTTP/JSON参数自动注入接口方法参数；
* 请求值要求json还是form、返回json还是返回页面 只需要注解即可搞定；request中的参数自动注入到接口方法参数中； 
* 拦截器配置极为简单，只需要简单注解。
* 统一的异常处理，代码中再也不需要管任何异常，直接抛出去就好，出错了框架会为你返回错误码。
  
## Installation
maven依赖：
```
<!-- https://mvnrepository.com/artifact/wang.moshu/smvc-framework -->
<dependency>
    <groupId>wang.moshu</groupId>
    <artifactId>smvc-framework</artifactId>
    <version>0.0.1</version>
</dependency>
```



## DEMO

运行smvc-demo，打开index.html运行测试例子。演示地址：http://123.206.202.189:8080/smvc-demo/

## Requirements

* java 6.0+ 


## License

SMVC is available under the Apache license, see the LICENSE file for more information.

## 使用指南

### 1.接口返回类型：页面或JSON
> 只需要在接口方法上指定returnType

#### 1.1 返回页面
```
	@RequestMapping(value = "", returnType = ReturnType.PAGE)
	public String index()
	{
		// 返回index页面
		return "index";
	}
```
#### 1.2 返回JSON
只需要在接口方法上指定returnType
```
	@RequestMapping(value = "returnData", returnType = ReturnType.JSON)
	public String returnData()
	{
		// 返回数据（框架自动打包为json）
		return "smvc maybe good.";
	}
```

### 2. 返回数据
#### 2.1 对于返回页面，在接口方法上指定增加Map类型参数，接口中增加入Map的值就是返回给页面的值
```
	@RequestMapping(value = "returnValue", returnType = ReturnType.PAGE)
	public String returnValue(Map<Object, Object> returnMap)
	{
		returnMap.put("returnValue1", 1);
		returnMap.put("returnValue2", 2);
		// 返回页面
		return "returnValue";
	}
```

#### 2.2 对于返回JSON，框架自动对返回值进行包装
接口：
```
	@RequestMapping(value = "returnData", returnType = ReturnType.JSON)
	public String returnData()
	{
		// 直接抛出异常
		return "smvc maybe good.";
	}
```
返回值：
```
{ "code":0, "data":"smvc maybe good.", "message":"", "returnCode":"" }
```

### 3.参数自动注入
#### 3.1 路由参数自动注入接口参数
> 注意：参数类型须为java基本对象型 
如下接口，路由中的name和id的值自动注入到接口name和id变量中。 
```
	@RequestMapping(value = "routeParamInject/{name}/{id}", returnType = ReturnType.PAGE)
	public String routeParamInject(String name, Integer id, Map<Object, Object> returnMap)
	{
		returnMap.put("name", name);
		returnMap.put("id", id);
		// 返回页面
		return "routeParamInject";
	}
```

#### 3.2 HTTP参数注入接口参数(java基本对象性)
接口
```
	@RequestMapping(value = "httpParamInject", returnType = ReturnType.PAGE)
	public String httpParamInject(String name, Integer id, Map<Object, Object> returnMap)
	{
		returnMap.put("name", name);
		returnMap.put("id", id);
		// 返回页面
		return "httpParamInject";
	}
```
url(post和get均支持)
```
http://localhost:8080/smvc-demo/p/httpParamInject?name=dingxy&id=1234
```

### 3.3 JSON参数注入接口参数(用户自定义对象性)
注解RequestMapping的requestDataType 变量默认为FORMDATA，也就是仅支持HTTP参数方式，如果需要支持JSON转换，需要指定**requestDataType = RequestDataType.JSON**

接口
```
	@RequestMapping(value = "jsonParamInject", requestDataType = RequestDataType.JSON, returnType = ReturnType.PAGE)
	public String jsonParamInject(HttpParamInjectRequest request, Map<Object, Object> returnMap)
	{
		Assert.notNull(request);
		returnMap.put("name", request.getName());
		returnMap.put("id", request.getId());
		// 返回页面
		return "httpParamInject";
	}
```
url(post和get均支持)
```
http://localhost:8080/smvc-demo/p/jsonParamInject?{"name":"dingxy","id":1234}
```

### 4.异常处理
#### 4.1 JSON接口异常处理
接口一切正常时返回json的code为0，其他异常情况为1。returnCode来自于抛出的BusinessException的errorcode，如果非BusinessException或者未指定errorcode，那么默认返回000001。

```
	@RequestMapping(value = "throwException", returnType = ReturnType.JSON)
	public void throwException()
	{
		// 直接抛出异常
		throw new BusinessException("101010", "接口抛出异常");
	}
```
返回值如下
```
{ "code":1, "data":"", "message":"接口抛出异常", "returnCode":"101010" }
```
#### 4.2 页面接口异常处理
页面接口发生异常均返回HTTP 500状态码，如果配置了errorpage那么会自动走到500对应页面上。

#### 4.3 访问不存在的接口
返回HTTP 404状态吗，如果配置了errorpage那么会自动走到404对应页面上。

### 5.业务service即加上注解就是接口，返回值就是json
接口
```
	@RequestMapping(value = "returnDataObj", returnType = ReturnType.JSON)
	public ReturnDataResponse returnDataObj(String name, Integer id)
	{
		Assert.notNull(name);
		Assert.notNull(id);

		ReturnDataResponse response = new ReturnDataResponse();
		response.setName(name);
		response.setId(id);

		return response;
	}
```
其中ReturnDataResponse为
```
public class ReturnDataResponse
{
	private String name;
	private Integer id;

        // getter and setter
}
```
请求URL
```
http://localhost:8080/smvc-demo/i/returnDataObj?name=dingxy&id=1234
```
返回值
```
{ "code":0, "data":{ "id":1234, "name":"dingxy" }, "message":"", returnCode":"" }
```

### 6.文件上传支持
直接将文件input域的名字作为接口方法参数或者请求参数的域。
单文件上传
```
	@RequestMapping(value = "oneFileUpload", returnType = ReturnType.JSON)
	public String oneFileUpload(MultipartFile file)
```
多文件上传(参数为MultipartFile数组)
```
	@RequestMapping(value = "multiFileUpload", returnType = ReturnType.JSON)
	public String multiFileUpload(MultipartFile[] file)
```

### 7.拦截器
#### 7.1 注解Intercept：作用目标为类和方法，表示该拦截器在该类或该方法对应的接口上工作
```
    @Intercept(value = { LoginInterceptor.class })
    public class DemoInterceptInterface
```
```
	@RequestMapping(value = "executeTime", returnType = ReturnType.JSON)
	@Clear(value = { LoginInterceptor.class })
	@Intercept(value = { ExecuteTimeInterceptor.class })
	public void executeTime()
	{
		// 该接口会被ExecuteTimeInterceptor拦截
	}
```
#### 7.2 注解Clear：作用目标方法，表示该拦截器在该方法对应的接口**不**工作
```
	@RequestMapping(value = "noNeedLogin", returnType = ReturnType.JSON)
	@Clear(value = { LoginInterceptor.class })
	public void noNeedLogin()
	{
		// 该接口执行不需要登陆
	}
```